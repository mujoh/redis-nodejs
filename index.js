var redis = require("redis");

// Create Redis client and pass host, port and password parameters
client = redis.createClient({host: '127.0.0.1', port: 6379, password: 'foobared'});

// On error display error
client.on("error", function (err) {
    console.log("Error " + err);
});

// On connect display Connected
client.on("connect", function() {
  console.log("Connected")
});

console.log("Basic assigning value to a key and printing with redis.print")
client.set("string key", "string val", redis.print);
client.get("string key", redis.print);

client.set("key01", "key01_value");
// This will return a JavaScript String
client.get("key01", function (err, reply) {
  if(err) console.log(err);
  console.log("key01 : " + reply.toString()); // Will print `key01 : key01_value`
});

// Sets key02 with value key02_value which expires in 10 seconds
client.set("key02", "key02_value", "EX", 10);
client.get("key02", redis.print);
// Print value of key02 after 10 seconds
setTimeout(function() {
    client.get("key02", redis.print); // Will return null
}, 11000);

//Set two hash keys and then output them
client.hset("hash key", "hashtest 1", "some value", redis.print);
client.hset(["hash key", "hashtest 2", "some other value"], redis.print);
client.hkeys("hash key", function (err, replies) {
  console.log(replies.length + " replies:");
  replies.forEach(function (reply, i) {
    console.log("    " + i + ": " + reply);
  });
});